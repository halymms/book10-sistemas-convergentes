const { isValidObjectId } = require('../database/mongoose');
const mongoose = require('../database/mongoose');

const LivroSchema = new mongoose.Schema({
  nomeLivro: String,
  autor: String,
  editora: String,
  edicao: Number,
});

module.exports = mongoose.model('Livros', LivroSchema);