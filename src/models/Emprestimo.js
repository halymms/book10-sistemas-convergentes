const mongoose = require('../database/mongoose');

const EmprestimoSchema = new mongoose.Schema({
  nomeLivro: String,
  data_emprestimo: Date,
  data_devolucao: Date,
});

module.exports = mongoose.model('Emprestimo', EmprestimoSchema);