const mongoose = require('../database/mongoose');

const ArtigoSchema = new mongoose.Schema({
  autor: String
});

module.exports = mongoose.model('Artigos', ArtigoSchema);