const mongoose = require('../database/mongoose');

const ClienteSchema = new mongoose.Schema({
  codigo: Int8Array,
  nome: String,
  idade: Int32Array,
  telefone: Int32Array,
  endereco: String,
  tipo_cliente: Int8Array
});

module.exports = mongoose.model('Clientes', ClienteSchema);