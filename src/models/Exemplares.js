const mongoose = require('../database/mongoose');

const ExemplareSchema = new mongoose.Schema({
  codigo: Int8Array,
  nome: String,
  tipoExemplar: Int32Array
});

module.exports = mongoose.model('Exemplares', ExemplareSchema);