const mongoose = require('../database/mongoose');

const PeriodicoSchema = new mongoose.Schema({
  editora: String,
});

module.exports = mongoose.model('Periodicos', PeriodicoSchema);