const customExpress = require('./config/custom-express');
const ReadYaml = require('./config/ReadYaml');

const app = customExpress();
const data = ReadYaml();

require('./controllers/index')(app);

app.listen(data['port'], () => console.log('Servidor rodando na porta 3333'));