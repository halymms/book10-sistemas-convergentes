const express = require('express')
const LivroSchema = require('../models/Livros');

const routes = express.Router();

routes.get('/listarLivros', async (request, response) => {
  const livros = await LivroSchema.find();

  return response.json(livros);
})

routes.post('/cadastrarLivros', async (request, response) => {
  const { nomeLivro, autor, editora, edicao } = request.body;

  const livros = await LivroSchema.create({
    nomeLivro,
    autor,
    editora,
    edicao
  })

  return response.json(livros);

})

module.exports = app => app.use('/livros', routes);