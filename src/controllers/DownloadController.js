const express = require("express");
const routes = express.Router();
const fs = require('fs');

routes.get('/download', async (request, response, next) => {
  const { nomeArquivo } = request.params;

  try{
      let readStream = fs.createReadStream(__dirname + '/../src/storage' + nomeArquivo)

      console.log('leu o arquivo');
      readStream.on('open', function () {
        readStream.pipe(response)
      })
  } catch(err) {
    const error = new Error();
      error.message = "Erro 001: Não foi possível realizar o download do arquivo"
      error.httpStatusCode = 404;
      error.code = 'ERROR001'
      return next(error)
  }
});

module.exports = app => app.use('/livros', routes);