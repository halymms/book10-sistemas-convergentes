const express = require("express")
const routes = express.Router();
const formidable = require('formidable');
const fsextra = require('fs-extra');


routes.post('/upload', async (request, response, next) => {
 
  let form = new formidable.IncomingForm();
  form.parse(request, function (err, fields, files) {
    const { type, name, path, size } = files.arquivo;
    
    if(type.indexOf("application/pdf") != -1) {
      fsextra.move(files.arquivo.path, './src/storage/' + files.arquivo.name, function () {
        console.log("success")
      })
  
      response.write('File uploaded.');
      response.end();
    } else {
      const error = new Error("Erro 002: Só é permitido upload de arquivo em PDF" );
      error.httpStatusCode = 400;
      error.code = 'ERROR002'
      return next(error)
    }
  });
});

module.exports = app => app.use('/livros', routes);
