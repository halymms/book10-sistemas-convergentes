const { response } = require('express');
const express = require('express');

const routes = express.Router();

routes.get('/livrosFavoritos', async(request, response) => {

  const books = [
    { book: "O Poder do Hábito", qtd: 46 },
    { book: "Pequeno Manual Antirracista", qtd: 37 },
    { book: "A Divina Comédia", qtd: 64 },
    { book: "Star Wars: Livro dos Sith", qtd: 13 },
    { book: "Led Zeppelin: Quando os gigantes caminhavam pela terra", qtd: 46 },
    { book: "Como as democracias morrem", qtd: 19 }
  ]

  const totalQtd = books.reduce( (prevVal, elem ) => prevVal + elem.qtd, 0 );

  return response.json(totalQtd);

} );

module.exports = app => app.use('/livros', routes);
