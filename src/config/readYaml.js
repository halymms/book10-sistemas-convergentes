
/*
console.log(__filename);
  // Prints: /Users/mjr/example.js
console.log(__dirname); */
module.exports = () => {
  const yaml = require('js-yaml');
  const fs = require('fs');
  let env = process.env.NODE_ENV;

  if (env == undefined) {
    env = 'dev';
  }

  let fileContents = fs.readFileSync(__dirname + '/../../'+ env +'.yaml', 'utf8');
  const data = yaml.safeLoad(fileContents);

  console.log(data['port']);

  return data;
}